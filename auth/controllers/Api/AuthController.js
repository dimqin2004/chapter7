const { User } = require('../../models')
const passport = require('../../lib/passport')

function format(user) {
  const { id, username } = user
  return {
    id, 
    username,
    accessToken: user.generateToken()
  }
}

const AuthController = {

  register: (req, res) => {
    User.register(req.body)
      .then(() => {
        res.json({
          message: 'User registered'
        })
      })
      .catch((error) => {
        res.render('error', error)
      })
    
  },

  login: passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/users/login',
    failureFlash: 'invalid username or password'
  }),

  loginJwt: (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        res.json(
          format(user)
        )
      })
      .catch((err) => {
        res.send(err.message)
      })
  },

  whoami: (req, res) => {
    const { user } = req
    res.json({
      id: user.dataValues.id,
      username: user.dataValues.username 
    })
  } 
}

module.exports = AuthController