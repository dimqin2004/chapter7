const { Room }= require('../../models')

const roomController = {
  create: (req, res) => {
    const { name } = req.body
    Room
      .create({name})
      .then((data) => {
        res.json({
          message: 'room created',
          data
        })
      })
      .catch((error) => {
        res.json({
          message: 'failed to create room',
          error
        })
      })

  } 
}

module.exports = roomController