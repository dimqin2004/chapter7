const {User, Users_history } = require('../../models')

const UserController = {
  findAll(req, res) {
    User
      .findAll()
      .then((data) => {
        res.json(data)
      })
      .catch((err) => res.json({
        message: 'error',
        errors: err
      }))
  },
  
  history(req, res) {
    const { id } = req.params
    User
      .findByPk(id, { include: { model: Users_history } })
      .then((data) => {
        res.json(data)
      })
  }
}

module.exports = UserController