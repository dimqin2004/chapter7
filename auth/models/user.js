'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Users_history, {
        foreignKey: 'UserId'
      })
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10)

    static register = ({username, password}) => {
      const encryptedPassword = this.#encrypt(password);

      return this.create({username, password: encryptedPassword});
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password)

    static authenticate = async ({username, password}) => {
      try {

        const user = await this.findOne({ where: {username} })
        if(!user) throw new Error('user not found')
        const isValidPassword = user.checkPassword(password)

        if (!isValidPassword) throw new Error('wrong password')
        return Promise.resolve(user)

      } catch(err) {
        console.log(err)
        Promise.reject(err)
      }

    }

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      }

      const options = {
        expiresIn: '1h'
      }
      
      const secret = 'ini rahasia';
      const token = jwt.sign(payload, secret, options);
      return token;
    }

  };
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};