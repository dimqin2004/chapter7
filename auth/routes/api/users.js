const express = require('express');
const router = express.Router();
const AuthController = require('../../controllers/api/AuthController')
const UserController = require('../../controllers/api/UserController')
const restrict = require('../../middleware/restrict')

/* GET users listing. */
router.get('/', restrict, UserController.findAll);

router.get('/:id/histories',UserController.history)
router.post('/register', AuthController.register)
router.post('/login', AuthController.loginJwt)
router.get('/profile', restrict, AuthController.whoami)

module.exports = router;
