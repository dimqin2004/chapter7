const express = require('express');
const router = express.Router();
const roomController = require('../../controllers/Api/RoomController')
const restrict = require('../../middleware/restrict')

router.post('/create',restrict, roomController.create)
module.exports = router